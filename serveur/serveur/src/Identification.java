import java.net.*;
import java.util.Scanner;
import java.io.*;

/**
 * This thread send the list of student, get the identifier of the student and launch communication
 * through the 2 ports by launching two thread Respond_Requets and Send_feedbacks
 * @author jade
 *
 */
public class Identification implements Runnable {

	private Socket socket_response;
	private Socket socket_continue;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private String login = null;
	public boolean authentifier = false;
	public Thread t2,t1;

	public Identification(Socket sr, Socket sc){
		socket_response = sr;
		socket_continue = sc;
	}
	public void run() {
		try {
			//initialize stream to communicate
			in = new BufferedReader(new InputStreamReader(socket_response.getInputStream()));
			out = new PrintWriter(socket_response.getOutputStream());
			out.flush();
			
			//send the list of student
			String list = getListStudent();
			out.println(list);
			out.flush();
			
			//get the identifier of the student
			login = in.readLine();
			System.out.println("L'élève "+login+" s'est connecté");
			
			//Thread to punctual request
			t2 = new Thread(new Respond_Request(socket_response,login));
			t2.start();	
			
			//Thread to continuous feedbacks
			t1 = new Thread(new Send_Feedbacks(socket_continue, login));
			t1.start();
		}
		catch (IOException e) 
		{
			System.err.println(login+" ne répond pas !");
		}		
	};

	/**
	 * This function return a list of student, read in the file Class
	 * @return String which is a list of student
	 */
	private String getListStudent()
	{
		String list ="";
		try {
			Scanner sc = new Scanner(new File("Class.txt"));
			while(sc.hasNext())
			{
				list = list+sc.next()+" ";
			}
			sc.close();
		} catch (FileNotFoundException e) {	
			System.err.println("Le fichier n'existe pas !");
		}
		return list;
	}
}