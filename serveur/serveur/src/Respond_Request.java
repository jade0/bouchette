import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

/**
 * When the application request a summary, it is this thread which handle it
 * The sending of summary is automatically generated, by reading a file containing the name of
 * all exercises to send
 * @author jade
 *
 */
public class Respond_Request implements Runnable {
	private Socket socket = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	private String login = "zero";
	private String message = null;
	private String[] list_summary = null;
	private int index_summary = 0;
	private boolean cancel = false;

	public Respond_Request(Socket s, String log){
		socket = s;
		login = log;
		getListSummary();
	}

	public void run() {
		try {

			//initialize streams
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			out.flush();

			while(!cancel){
				try {
					message = in.readLine();
					System.out.println("le message est : " +message);
					if (message.contentEquals("summary")) //check if it is the right request
					{
						String summary = getSummary();
						System.out.println("j'envoie " + summary);
						out.println(summary);
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}   
		} catch (IOException e) {
			System.err.println(login +"s'est déconnecté ");
		}
	}

	/**
	 * This function parse the file Summary and convert it into a list of summary
	 */
	private void getListSummary()
	{
		String list ="";
		try {
			Scanner sc = new Scanner(new File("Summary.txt"));

			while(sc.hasNext())
			{
				list = list+sc.next()+" ";
			}
			sc.close();
			String delims = "[ ]+";
			list_summary = list.split(delims);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This function return the current summary inside the list
	 * @return String
	 */
	private String getSummary()
	{
		String list ="";
		try {
			if (list_summary == null)
			{
				System.out.println("je suis l'index :"+index_summary);
				cancel=true; //stop responding request after this one
				return "end_session";
			}
			else
			{
				Scanner sc = new Scanner(new File(list_summary[0]));
				while(sc.hasNext())
				{
					list = list+sc.next()+" ";
				}
				sc.close();
				if (list_summary.length == 1)
				{
					list_summary = null;
				}
				else
				{
					list_summary = Arrays.copyOfRange(list_summary, 1, list_summary.length);
				}
			}
		} catch (FileNotFoundException e) {	
			System.err.println("Le fichier n'existe pas !");
		}
		return list;
	}
}