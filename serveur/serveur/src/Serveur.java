import java.io.*;
import java.net.*;
import java.util.Properties;


/**
 * This is the main program 
 * it loads the properties, and accepts connections from two port
 * @author jade
 *
 */
public class Serveur {
	public static ServerSocket socketResponse = null;
	public static ServerSocket socketContinue = null;
	public static int port_response;
	public static int port_continue;
	public static Thread t1,t2;
	public static Properties prop ;
	public static void main(String[] args) {	

		loadProp();

		try {   
			socketResponse = new ServerSocket(port_response);
			System.out.println("Le serveur est à l'écoute du port "+socketResponse.getLocalPort());
			socketContinue = new ServerSocket(port_continue);
			System.out.println("Le serveur est à l'écoute du port "+socketContinue.getLocalPort());

			t1 = new Thread(new Accept_connection(socketResponse,socketContinue));
			t1.start();
		} catch (IOException e) {
			System.err.println("Le port +socketResponse.getLocalPort()+ou +socketContinue.getLocalPort()+ est déjà utilisé !");
		}
	}

	public static void loadProp()
	{
		prop = new Properties();
		String fileName = "config.properties";

		try {
			InputStream is = new FileInputStream(fileName);
			try {
				prop.load(is);
				port_response =Integer.parseInt(prop.getProperty("num_port_response"));
				port_continue =Integer.parseInt(prop.getProperty("num_port_continue"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}