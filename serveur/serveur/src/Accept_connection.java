import java.io.*;
import java.net.*;

/**
 * This thread is running when the server launch it.
 * It accepts connection in continue and launch Identification
 * @author jade
 *
 */
public class Accept_connection implements Runnable{

	private ServerSocket socketserver_response = null;
	private Socket socket_response = null;
	private ServerSocket socketserver_continue = null;
	private Socket socket_continue = null;

	public Thread t1;
	public Accept_connection(ServerSocket sr,ServerSocket sc){
		socketserver_response = sr;
		socketserver_continue = sc;
	}

	public void run() {
		try {
			while(true){
				socket_response = socketserver_response.accept();
				socket_continue = socketserver_continue.accept();
				System.out.println("Un élève veut se connecter  ");

				t1 = new Thread(new Identification(socket_response,socket_continue));
				t1.start();
			}
		} catch (IOException e) {
			System.err.println("Erreur serveur");
		}
	}
}