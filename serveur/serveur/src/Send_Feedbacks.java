import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Scanner;

/**
 * This thread is used to send feedback to the student
 * Messages to send have to be printed in the console, according to a protocol
 * a message is a string followed by the button 'enter'
 * Only one message by activity (errors otherwise, because other lines will be send
 * to the following activity)
 * @author jade
 *
 */
public class Send_Feedbacks implements Runnable {
	private Socket socket = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	private String login = "zero";
	public Integer activity =0;
	private boolean cancel = false;

	public Send_Feedbacks(Socket s, String log){
		socket = s;
		login = log;
	}

	public void run() {
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("je commence à ecouter");

		Scanner sc =  new Scanner(System.in);
		while(!cancel){
			try {
				/*get the number of the activity
					0 => init
					1 => begin_exercise
					2 => exercise
					3 => end_exercise
					4 => fin_session
				*/
				String activity_message = in.readLine();
				if (activity_message !=null)
				{
					activity = Integer.parseInt(activity_message);
					System.out.println(login+" est dans l'activité : "+activity);
					if (activity !=0)
					{
						System.out.println("Votre message dans "+activity+" :");
						String message = sc.nextLine(); //write the feedback to send
						out.println(activity+message);
						out.flush();
					}
					else if(activity == 4) //end of the session
					{
						System.out.println("close send_feedbacks");
						cancel = true;
						sc.close();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		sc.close();
	}

}
