\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Presentation}{3}{0}{1}
\beamer@sectionintoc {2}{Issues}{5}{0}{2}
\beamer@sectionintoc {3}{Informatic Environment for Human Learning}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Description of project}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Existing devices}{11}{0}{3}
\beamer@subsectionintoc {3}{3}{Didactic modele}{13}{0}{3}
\beamer@sectionintoc {4}{Application}{14}{0}{4}
\beamer@subsectionintoc {4}{1}{Graphical User Interface}{16}{0}{4}
\beamer@subsectionintoc {4}{2}{Modelisation with server}{17}{0}{4}
\beamer@sectionintoc {5}{Experimentations}{18}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{21}{0}{6}
