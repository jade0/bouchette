\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {paragraph}{Didactic}{1}
\contentsline {paragraph}{Feedbacks}{1}
\contentsline {section}{\numberline {2}Context}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {3}Material}{2}
\contentsline {paragraph}{}{2}
\contentsline {subsection}{\numberline {3.1}Tangible environment}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{Constraints of the scales \\}{3}
\contentsline {paragraph}{Connection to others devices \\}{3}
\contentsline {subsection}{\numberline {3.2}Exercises}{3}
\contentsline {paragraph}{}{3}
\contentsline {subsection}{\numberline {3.3}Application}{3}
\contentsline {paragraph}{}{3}
\contentsline {subsection}{\numberline {3.4}Feedback interactions}{4}
\contentsline {paragraph}{}{4}
\contentsline {subsection}{\numberline {3.5}Simulation}{4}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {4}Improvement}{5}
\contentsline {paragraph}{ }{5}
\contentsline {section}{\numberline {5}Evaluation of the project}{5}
\contentsline {subsection}{\numberline {5.1}Protocol}{5}
\contentsline {paragraph}{ }{5}
\contentsline {subsection}{\numberline {5.2}Experiments}{5}
\contentsline {paragraph}{ }{5}
\contentsline {section}{\numberline {6}Further evaluations}{5}
\contentsline {paragraph}{ }{5}
\contentsline {subsection}{\numberline {6.1}Usability criterion}{5}
\contentsline {subsection}{\numberline {6.2}Utility criterion}{5}
\contentsline {section}{\numberline {7}Conclusion}{6}
\contentsline {section}{\numberline {A}Screen of the application}{7}
\contentsline {section}{\numberline {B}Format of traces send by the Raspberry Pi}{8}
\contentsline {section}{\numberline {C}Format of JSON object send by the Raspberry Pi}{9}
\contentsline {section}{\numberline {D}Format of exercise}{10}
\contentsline {section}{\numberline {E}Schemas}{10}
