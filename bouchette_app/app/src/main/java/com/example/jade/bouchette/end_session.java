package com.example.jade.bouchette;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by jade on 03/06/15.
 * This activity is accesible when the session need to terminate
 * It could becausethere is no more exercise, or a connexion error
 * In this activity, the application :
 *      Displays a text that explains the reason to be in this activity
 *      Proposes a button to quit the application
 */

public class end_session extends ActionBarActivity {
    /**
     * Button listener to quit the application
     */
    private View.OnClickListener buttonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            finish();
            System.exit(0);
        }
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin_session);
        TextView textview = (TextView)findViewById(R.id.text_end);

        Intent intent = getIntent();
        String text = intent.getStringExtra("text"); //text explains which event has trigger this activity
        textview.setText(text);
        utils util =  new utils();
        util.setFont(textview,getAssets());

        Button end = (Button) findViewById(R.id.end_button);
        end.setOnClickListener(buttonListener);
        util.setFont(end,getAssets());
    }
}