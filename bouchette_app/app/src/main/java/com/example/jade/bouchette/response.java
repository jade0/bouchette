package com.example.jade.bouchette;

import java.io.Serializable;

/**
 * Created by jade on 15/06/15.
 * Class to represent the response of the student, either with number of with letters
 */
public  class response implements Serializable {
    private boolean type = false;
    public boolean getType()
    {
        return type;
    }
    private int num = -1;
    private String letters = "";

    /**
     * The response is either in letters or in number
     * @param type of the response : true if response in letters, false otherwise
     */
    public response(boolean type, int num, String lett)
    {
        this.type= type;
        this.letters = lett;
        this.num = num;
    }
    public String getLetters() { return letters; }
    public int getNumber() {
        return num;
    }
}
