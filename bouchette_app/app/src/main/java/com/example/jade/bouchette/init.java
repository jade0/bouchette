package com.example.jade.bouchette;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jade.client.Client;
import com.example.jade.client.Client_demo;
import com.example.jade.client.Client_reel;
import com.example.jade.client.Reception_continue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;


public class init extends ActionBarActivity {
    private Spinner list_student = null;
    private Button acces = null;
    public Client client;
    private int id_student = 0;
    private Handler handler;
    private utils util;
    private Toast toast;
    private List<student> list = new ArrayList<student>();

    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // vérification si l'élève a choisi son nom
            if (id_student == 0) { util.setToast("Tu dois sélectionner ton nom",init.this, toast); }
            else {
                try {
                    client.setId(id_student);
                } catch (IOException e) {
                    e.printStackTrace();
                    Intent intent = new Intent(init.this, end_session.class);
                    client.close();
                    intent.putExtra("text","Probleme de connexion avec le server");
                    startActivity(intent);
                }
                Intent intent;
                if(!util.demo) {
                    handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            Intent intent1 = new Intent("my-event");
                            // add data
                            intent1.putExtra("message", client.message_continue);
                            if (client.message_continue == null) {
                                System.out.println("je dois planter");
                            }
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent1);
                        }
                    };
                    BufferedReader in = null;
                    try {
                        System.out.println("client " + client.toString());
                        in = new BufferedReader(new InputStreamReader(client.socket_continue.getInputStream()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Reception_continue reception = new Reception_continue(in, client, handler, init.this);
                    new Thread(reception).start();
                    intent = new Intent(init.this, begin_exercise.class);
                }
                else
                {
                    intent = new Intent(init.this, choice_exercise.class);
                }

                Bundle bundle = new Bundle();
                bundle.putSerializable("client", client);
                intent.putExtras(bundle);
                bundle.putSerializable("util", util);
                intent.putExtras(bundle);
                toast.cancel();
                startActivity(intent);
                finish();
            }
        }
    };

    @Override
    public void onBackPressed() {
        // do nothing.
    }

    private OnItemSelectedListener spinnerListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String name = (String) parent.getSelectedItem();
            for (Iterator it = list.iterator(); it.hasNext(); ) {
                student student = (com.example.jade.bouchette.student) it.next();
                if (student.getName().contentEquals(name)) {
                    id_student = student.getId();
                    return;
                }
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);

        list_student = (Spinner) findViewById(R.id.list_student);
        acces = (Button) findViewById(R.id.acces);
        id_student = 1;


    };
    @Override
    protected void onStart() {
        super.onStart();
        toast = new Toast(getApplicationContext());
        getProperties();
        add_student();
        addListenerOnSpinner();
        addListenerOnButton();
        TextView text = (TextView) findViewById(R.id.Choice_name);
        util.setFont(text, getAssets());
        util.setFont(acces, getAssets());
        client.setActivity(0, init.this);
    }

    public void getProperties()
    {
        try {
            AssetManager assetManager=getAssets();
            InputStream inputStream = assetManager.open("raw/config.properties");
            Properties prop = new Properties();
            prop.load(inputStream);
            System.out.println("The properties are now loaded");
            System.out.println("properties: " + prop);
            util =new  utils(prop.getProperty("url_miniPC"),prop.getProperty("ip_serveur"),prop.getProperty("lecture_enonce"),
                    prop.getProperty("lecture_aide"),prop.getProperty("ecriture"),
                    Integer.parseInt(prop.getProperty("num_port_response")),
                    Integer.parseInt(prop.getProperty("num_port_continue")),prop.getProperty("retro_action"),Boolean.parseBoolean(prop.getProperty("demo")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(util.demo)  {client = new Client_demo(); }
        else {
            client = new Client_reel();
            client.setUtil(util);
        }
        System.out.println("je suis le client "+client.getClass());

    }
    public void addListenerOnSpinner() {
        list_student.setOnItemSelectedListener(spinnerListener);
    }

    public void addListenerOnButton() {
        acces.setOnClickListener(buttonListener);
    }

    public void add_student() {
        student_list student = new student_list(this);
        try {
            list = student.execute(client).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        List<String> list_name = new ArrayList<String>();
        for (int i = 0; i < list.size(); i++) {
            com.example.jade.bouchette.student tmp = list.get(i);
            list_name.add(tmp.getName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list_name);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        list_student.setAdapter(dataAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_init, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

