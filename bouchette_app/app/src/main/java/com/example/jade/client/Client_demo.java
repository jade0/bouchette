package com.example.jade.client;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * This class extends Client, and an instance of this class is created when there is no connexion with the server
 * It is only for purpose demonstration
 */
public class Client_demo extends Client {

    private int[] index_summary ; //list of summaries, chosen by the user in the choice_exercice layout.

    public int[] getIndex_summary() {
        return index_summary;
    }

    public void setIndex_summary(int[] index_summary) {
        this.index_summary = index_summary;
    }

    public void connect() throws IOException {}

    public void setId(int id) throws IOException{ }

    public void setActivity(int activity_id, Activity activity)
    {
    }

    /**
     *  Return a fake list of student
     * @param assetManager because list of student is stored inside asset folder
     * @return
     */
    public String getList(AssetManager assetManager)
    {
        String list ="";
        try {
            InputStream inputStream = assetManager.open("raw/Classe.txt");
            Scanner sc = new Scanner(inputStream);

            while(sc.hasNext())
            {
                list = list+sc.next()+" ";
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  list;
    }

    /**
     * @param assetManager because summary is stored inside asset folder
     */
    @Override
    public void getSummary(AssetManager assetManager) throws IOException
    {
        try {
            String name = "raw/Summary"+index_summary[0]+".txt";
            InputStream inputStream = assetManager.open(name);
            Scanner sc = new Scanner(inputStream);
            String list ="";
            while(sc.hasNext())
            {
                list = list+sc.next()+" ";
            }
            sc.close();
            message_response = list;

            //remove the current summary from the list
            int[] withoutFirst = Arrays.copyOfRange(getIndex_summary(), 1, getIndex_summary().length);
            setIndex_summary(withoutFirst);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}