package com.example.jade.client;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import com.example.jade.bouchette.end_session;
import com.example.jade.bouchette.utils;
import java.io.*;
import java.net.*;

/**
 * This class extends Client, and an instance of this class is created when there is a connexion with the server
 */
public class Client_reel extends Client implements Serializable {

	private utils util;

	public void setUtil(utils util){ this.util = util;}

	public void connect() throws IOException
	{
		System.out.println("Demande de connexion 2009 "+ util.ip_serveur);
		InetAddress serverAddr = InetAddress.getByName(util.ip_serveur);

		socket_reponse = new Socket(serverAddr,util.num_port_response);
		System.out.println("Connexion établie avec le serveur, authentification :"); // Si le message_response s'affiche c'est que je suis connecté

		System.out.println("Demande de connexion 2010");
		socket_continue = new Socket(serverAddr,util.num_port_continue);
		System.out.println("Connexion établie avec le serveur, authentification :"); // Si le message_response s'affiche c'est que je suis connect
	}

	public void setId(int id) throws IOException
	{
			PrintWriter out = new PrintWriter(socket_reponse.getOutputStream());
			out.flush();
			out.println(id);
			out.flush();
			this.id = id;
	}

	public void setActivity(int activity_id, Activity activity)
	{
		try {
			PrintWriter out1 = new PrintWriter(socket_continue.getOutputStream());
			out1.flush();
			out1.println(activity_id);
			out1.flush();
		} catch (IOException e) {
			//launch a new activity to alert the user
			e.printStackTrace();
			Intent intent = new Intent(activity, end_session.class);
			this.close();
			intent.putExtra("text","Probleme de conexion avec le serveur");
			activity.startActivity(intent);
			activity.finish();
		}
	}
	public String getList(AssetManager assetManager)
	{
		Connexion connect = new Connexion(socket_reponse);
		String message = "";
		try {
			message = connect.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  message;
	}

	public void getSummary(AssetManager asset) throws IOException
	{
		PrintWriter out = new PrintWriter(socket_reponse.getOutputStream());
		BufferedReader in = new BufferedReader(new InputStreamReader(socket_reponse.getInputStream()));
		out.println("summary");
		out.flush();
		message_response = in.readLine(); //the server send a String which represents all the summary
	}
}