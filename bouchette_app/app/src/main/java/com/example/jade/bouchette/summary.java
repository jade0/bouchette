package com.example.jade.bouchette;

import java.util.ArrayList;

/**
 * Created by jade on 30/04/15.
 * Class that represents a summary
 */
public class summary {
    private String text_read = ""; //text to display on the screen
    private String text_spoken = ""; //text to read aloud
    public ArrayList<scale> initial_stick ; // list of scales (identifiant and content)
    public  ArrayList<response> response_list; //list of awaited responses for each field
    private boolean check; //true if initial conditions have to be checked at the beginning of the exercisee

    public void summary()
    {
        this.initial_stick = new ArrayList<scale>();
        this.response_list = new ArrayList<response>() ;
    }
    
    public boolean getCheck() { return check; }
    public ArrayList<response> getResponse_list() { return response_list;  }
    public String getText_read() { return this.text_read; }
    public String getText_spoken() { return this.text_spoken; }
    public ArrayList<scale> getIStick() { return this.initial_stick;}

    public void setResponse_list(ArrayList<response> response_list) { this.response_list = response_list; }
    public void setText_read(String text) {  this.text_read=text; }
    public void setText_spoken(String text) {  this.text_spoken=text; }
    public void setIStick(ArrayList<scale> array) {  this.initial_stick = array; }
    public void setCheck(boolean check) {this.check = check;   }
}
