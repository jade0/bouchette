package com.example.jade.bouchette;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jade.client.Client;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Created by Jade on 07/04/2015.
 * In this activity, the application :
 *      recovers the wording
 *      checks the initial conditions
 *      launchs the exercise
 */
public class begin_exercise extends ActionBarActivity implements TextToSpeech.OnInitListener{
    public Client client = null;
    public summary summary= new summary();
    private Button start = null;
    private TextView textview = null;
    private utils util;
    public TextToSpeech tts;
    private ParametersRequest parameters = null;
    private int MY_DATA_CHECK_CODE = 0;
    private Toast toast;
    /**
     * Button listener to begin the exercice
     */
    private View.OnClickListener buttonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            //write trace
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";no_erreur_balance\n";
            util.write(trace, getApplicationContext());

            //send paramters
            Intent intent = new Intent(begin_exercise.this, exercise.class);
            intent.putExtra("text", summary.getText_read());
            intent.putExtra("vocal", summary.getText_spoken());
            Bundle bundle=new Bundle();
            bundle.putSerializable("client", client);
            intent.putExtras(bundle);
            bundle.putSerializable("util", util);
            intent.putExtras(bundle);
            intent.putExtra("response_list", summary.getResponse_list());
            toast.cancel();

            //launch new activity and close this one
            startActivity(intent);
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
            finish();
        }
    };

    /**
     * if the initial conditions aren't respected, pushing the button triggered this callback
     */
    private View.OnClickListener erreur = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            //write trace
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";erreur_balance\n";
            util.write(trace, getApplicationContext());

            //verify content of the scale
            check_scale();
            client.setActivity(1, begin_exercise.this);

            if(textview.getText().length() == 0) //without mistake
            {
                textview.setVisibility(View.INVISIBLE);
                start.setOnClickListener(buttonListener);
            }
            else //with mistake, show toast
            {
                util.setToast("Tu dois mettre les balances et les bâtons correctement", begin_exercise.this, toast );
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_exercice);

        //recover datas
        Intent intent=this.getIntent();
        Bundle bundle = intent.getExtras();
        client =(Client)bundle.getSerializable("client");
        util = (utils)bundle.getSerializable("util");

        start = (Button)findViewById(R.id.begin_button);
        start.setOnClickListener(buttonListener);

        textview = (TextView)findViewById(R.id.begin_text);
        textview.setText("");
        util.setFont(textview, getAssets());
    }

    /**
     * Control the number of sticks inside the tangible system
     */
    private void check_scale()
    {
        RequestMiniPC task =  new RequestMiniPC(begin_exercise.this);
        try {
            task.execute(parameters).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param status of the control of Text to Speech installationn
     */
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.FRANCE);
        }
    }
    @Override
    public  void onStop()
    {
        if (tts != null) {
            tts.stop();
        }
        super.onStop();
    }
    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
        System.gc();
        client.close();
    }


    @Override
    protected void onStart() {
        super.onStart();
        toast = new Toast(getApplicationContext());

        if(get_wording(client)) {
            //enter here if the system achieves to get the wording

            if (tts == null) {
                Intent checkIntent = new Intent();
                checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
                startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
                tts = new TextToSpeech(this, this);
            }
            //register the message receiverfrom the server
            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("my-event"));

            parameters = new ParametersRequest(util.url_miniPC, summary, textview);

            start.setOnClickListener(buttonListener);
            util.setFont(start, getAssets());

            if(summary.getCheck()) //if checking scales is requested
            {
                check_scale();
                if (textview.getText().length() != 0) { start.setOnClickListener(erreur); }
            }

            start.setVisibility(View.VISIBLE);
            client.setActivity(1, begin_exercise.this);
        }
    }

    /**
     * handle message from the server
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            util.setHandler(1, message, begin_exercise.this, textview, tts, null, null,toast);
            client.message_continue = "";
        }
    };

    /**
     * Request the wording to the client
     * @param client (reel or for demonstration)
     * @return  true (if the system get a wording)
     *          false: launch the end of the session
     */
    public boolean get_wording(Client client)
    {
        wording wording1 = new wording(this.getAssets());
        try {
            summary = wording1.execute(client).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            Intent intent = new Intent(begin_exercise.this, end_session.class);
            intent.putExtra("text","Probleme de connexion avec le serveur");
            client.close();
            startActivity(intent);
            finish();
            return false;
        }
        if (summary == null) {
            System.out.println("fin de la session");
            Intent intent = new Intent(begin_exercise.this, end_session.class);
            client.close();
            intent.putExtra("text","Fin de la session");
            startActivity(intent);
            finish();
            return false;
        }
        else { return true; }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                if (tts == null){
                    System.out.println("tts est null");
                    //  tts = new TextToSpeech(this, this);
                    // tts.stop();
                }
            }
            else {
                // missing data, install it
                //ATTENTION: BELOW THIS GIVES ME PROBLEMS SINCE IT OPENS MARKET
                //AND I HAVE TO HIT THE BACK BUTTON, THEN, IT SPEAKS!
                //BTW TTS ENGINE "IS" INSTALLED!!
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

}
