package com.example.jade.client;
import java.net.*;
import java.io.*;
import java.util.concurrent.Callable;

/**
 * This thread is running when the list of student is requested
 */
public class Connexion implements Callable<String> {

	private Socket socket = null;
	private boolean connect = false;
	private String message;
	public Connexion(Socket s)
	{
		socket = s;
	}

	@Override
	public String call() throws Exception {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			while(!connect )
			{
				message = in.readLine(); //recuperer la liste des eleve
				System.out.println("J'ai la liste des élèves");
				connect = true;
			}
		} catch (IOException e) {
			System.err.println("Le serveur ne répond plus ");
		}
		return message;
	}
}