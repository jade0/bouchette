package com.example.jade.bouchette;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import com.example.jade.client.Client;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This Task gets the text send by the server,and parses it into a summary
 */
public class wording extends AsyncTask<Client, Void, summary>
{
    AssetManager asset;
    public wording(AssetManager asset)
    {
        this.asset = asset;
    }

    protected summary doInBackground(Client... client) {

        //request the summary
        try {
            client[0].getSummary(asset);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (client[0].message_response == null) {
            System.out.println("le message_response est null");
            return null;
        }

        String list = client[0].message_response;
        String delims = "[&]+";
        String[] params = list.split(delims);
        summary summary = new summary();
        summary.setIStick(new ArrayList<scale>());
        summary.setResponse_list(new ArrayList<response>());

        if (params[0].contentEquals("end_session")) {
            return null;
        } else {
            //get the value of the summary
            summary.setText_read(params[1]); //wording
            summary.setText_spoken(params[2]); //text to read aloud

            //awaited responses
            if(!params[3].contentEquals("null")) {
                System.out.println("param "+params[3]);
                delims = "[;]+";
                String[] response = params[3].split(delims);
                System.out.println(response[0]);
                for (int i = 0; i < response.length; i++) {
                    delims = "[,]+";
                    String[] details = response[i].split(delims);
                    response tmp;
                    if (!Boolean.parseBoolean(details[0])) {
                        tmp = new response(false, Integer.parseInt(details[1]), "");
                    } else {
                        tmp = new response(true, 0, details[1]);
                    }
                    summary.getResponse_list().add(tmp);
                }
            }

            //boolean true if checking the initial conditions is needed
            summary.setCheck(Boolean.parseBoolean(params[4]));

            //list of scales
            delims = "[;]+";
            String[] balance = params[5].split(delims);
            for (int i = 0; i < balance.length; i++) {
                delims = "[,]+";
                String[] details = balance[i].split(delims);
                scale tmp = new scale(details[0], Integer.parseInt(details[1]), Integer.parseInt(details[2]));
                summary.getIStick().add(tmp);
            }

            return summary;
        }
    }
}


