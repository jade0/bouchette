package com.example.jade.bouchette;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jade.client.Client;

/**
 * Created by jade on 16/06/15.
 * This activity is only accessible in demonstration mode
 * In this activity, the application :
 *      makes the user choose his/her session of exercise
 */
public class choice_exercise extends ActionBarActivity {
    private Client client;
    private utils util;
    private int[] list; //list of exercises

    private View.OnClickListener s11_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            list = new int[]{111, 112};
            client.setIndex_summary(list);
            begin();
        }
    };
    private View.OnClickListener s12_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            list = new int[]{121, 122};
            client.setIndex_summary(list);
            begin();
        }
    };
    private View.OnClickListener s21_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            list = new int[]{211, 212,213};
            client.setIndex_summary(list);
            begin();
        }
    };
    private View.OnClickListener s22_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            list = new int[]{221, 222,223};
            client.setIndex_summary(list);
            begin();
        }
    };
    private View.OnClickListener s23_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            list = new int[]{231, 232,233};
            client.setIndex_summary(list);
            begin();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);

        Button s11 = (Button) findViewById(R.id.s1_1);
        s11.setOnClickListener(s11_listener);
        Button s12 = (Button) findViewById(R.id.s1_2);
        s12.setOnClickListener(s12_listener);
        Button s21 = (Button) findViewById(R.id.s2_1);
        s21.setOnClickListener(s21_listener);
        Button s22 = (Button) findViewById(R.id.s2_2);
        s22.setOnClickListener(s22_listener);
        Button s23 = (Button) findViewById(R.id.s2_3);
        s23.setOnClickListener(s23_listener);

        Intent intent=this.getIntent();
        Bundle bundle = intent.getExtras();
        client =(Client)bundle.getSerializable("client");
        util = (utils)bundle.getSerializable("util");

        util.setFont((TextView)findViewById(R.id.s1), getAssets());
        util.setFont((TextView)findViewById(R.id.s2), getAssets());
        util.setFont(s11, getAssets());
        util.setFont(s12, getAssets());
        util.setFont(s21, getAssets());
        util.setFont(s22, getAssets());
        util.setFont(s23, getAssets());

    }

    /**
     * Launch the activity and send datas
     */
    public void begin()
    {
        Intent intent = new Intent(choice_exercise.this, begin_exercise.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("client", client);
        intent.putExtras(bundle);
        bundle.putSerializable("util", util);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
