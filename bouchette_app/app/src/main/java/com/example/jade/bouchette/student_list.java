package com.example.jade.bouchette;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.jade.client.Client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class student_list extends AsyncTask<Client,Void,List<student>>
{
    private Activity activity;
    public student_list(Activity activity)
    {
        this.activity = activity;
    }

    public List<student> list = new ArrayList<student>();
    @Override
    protected List<student> doInBackground(Client... c) {
        try {
            c[0].connect();
        }catch (IOException e) {
            Intent intent = new Intent(activity, end_session.class);
            intent.putExtra("text","Probleme de connexion avec le server");
//            c[0].close();
            activity.startActivity(intent);
            activity.finish();
            e.printStackTrace();
        }

        String list1 = c[0].getList(activity.getAssets());
        String delims = "[ ]+";
        String[] tokens = list1.split(delims);

        student student0 = new student("----------------------", 0);
        list.add(student0);
        for (int i = 0; i <tokens.length ; i=i+3)
        {
            student student =new student(tokens[i+1] + " " + tokens[i+2], Integer.parseInt(tokens[i]));
            list.add(student);
        }
        return list;
    }

    protected void onPostExecute(List<String> list) {

    }
}

