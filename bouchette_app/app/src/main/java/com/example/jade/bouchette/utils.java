package com.example.jade.bouchette;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by jade on 26/05/15.
 * Class that regroupsevery usefull fonctions, shared by several activies
 */
public class utils implements Serializable{
    //parameters of the configuration
    public  String url_miniPC ;
    public  String ip_serveur ;
    public  String lecture_enonce;
    public  String lecture_aide ;
    public  String ecriture;
    public  String retro_action;
    public  Integer num_port_response ;
    public  Integer num_port_continue ;
    public boolean demo =false;

    public utils(){}

    public utils(String url_miniPC,String ip_serveur ,String lecture_enonce,String lecture_aide,String ecriture,Integer num_port_response, Integer num_port_continue , String retro_action, boolean demo )
    {
        this.url_miniPC = url_miniPC;
        this.ip_serveur = ip_serveur;
        this.lecture_enonce = lecture_enonce;
        this.lecture_aide = lecture_aide;
        this.ecriture = ecriture;
        this.num_port_continue = num_port_continue;
        this.num_port_response = num_port_response;
        this.retro_action = retro_action;
        this.demo = demo;
    }

    /**
     * This fonction modify the font of a textview
     * @param text text to modify
     * @param asset asset of the current activity
     */
    public void setFont(TextView text, AssetManager asset)
    {
        Typeface textfont = Typeface.createFromAsset(asset,"fonts/euphorigenic.ttf");
        text.setTypeface(textfont);
    }

    /**
     * This fonction modify the font of the text of a button
     * @param text text to modify
     * @param asset asset of the current activity
     */
    public void setFont(Button text, AssetManager asset)
    {
        Typeface textfont = Typeface.createFromAsset(asset,"fonts/euphorigenic.ttf");
        text.setTypeface(textfont);
    }

    /**
     * Write traces inside a file
     * @param trace text of the trace to store
     * @param context context of the activity
     */
    public void write(String trace, Context context ) {
        File file = new File(context.getFilesDir(), "trace.txt");
        try
        {
            FileWriter writer = new FileWriter(file, true);
            writer.write(trace);
            writer.flush();
            writer.close();
        }catch(  IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * This fonction handle a message send by the server.
     * According to a protocole, it parses the message and reacts to it
     * @param activity_id identifier of the activity
     * @param message TString send by the server
     * @param activity Current activity
     * @param textview Text of the wording
     * @param tts Texttospeech used to speak
     * @param speak button to listen again the wording
     * @param send button, usefull to arrange the interface layout
     */
    public void setHandler(int activity_id, String message, Activity activity,TextView textview,TextToSpeech tts, Button speak, Button send, Toast toast)
    {
        if(message == null) { activity.finish();  }
        else if(activity_id == Integer.parseInt(message.substring(0,1)))
        {
            message = message.substring(1,message.length());
            if (message.startsWith(lecture_enonce)) {
                String text_read = (String)textview.getText();
                if(activity_id == 2) { setSpeech_with_button(tts, speak, send, text_read); }
                else { setHelp(text_read, tts);   }
            }
            if (message.startsWith(lecture_aide)) {
                String text_read = (String)message.subSequence(this.lecture_aide.length(), message.length());
                setHelp(text_read,tts);
            }
            if (message.startsWith(ecriture)) {
                String text_read = (String)message.subSequence(this.ecriture.length(), message.length());
                setToast(text_read, activity, toast);
            }
            if (message.startsWith(retro_action)) {
                setRetro(message,textview);
            }
        }
    }

    /**
     * Read aloud the wording, makes a listening button and adjust the layout
     * @param tts TexttoSpeech used to speak
     * @param speak button to listen the wording
     * @param send button to send the result
     * @param text_read Text to read
     */
    public void setSpeech_with_button(TextToSpeech tts, Button speak, Button send, String text_read) {

        if (!text_read.contentEquals("null")) {
            tts.stop();
            speak.setVisibility(View.VISIBLE);
            TableLayout.LayoutParams table = new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 2f);
            table.setMargins(0, 0, 0, 16);
            send.setLayoutParams(table);
            speak.setLayoutParams(new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 1f));
            tts.speak(text_read, TextToSpeech.QUEUE_ADD, null);
        }
    }


    /**
     * This function reads aloud a text
     * @param text_read message send by the server
     * @param tts texttospeech to use
     */
    public void setHelp(String text_read, TextToSpeech tts)
    {
        tts.stop();
        tts.speak(text_read, TextToSpeech.QUEUE_ADD, null);
    }

    /**
     * This function makes a Toast pop inside the screen
     * @param message text to displays
     * @param activity current actitivy
     */
    public void setToast(String message, Activity activity, Toast toast)
    {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);
        setFont(text, activity.getAssets());

        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /**
     * This function replaces the current text of a textview to the text enters as parameters
     * @param message text sent by the server
     * @param textview textView to display the text
     */
    private void setRetro(String message, TextView textview) {
        String display = (String)message.subSequence(retro_action.length(),message.length());
        textview.setText(display);
    }

}
