package com.example.jade.bouchette;

/**
 * Class that represent the entity student, which is a name and a personnal id
 */
public class student
{
    private String name;
    private int id;

    student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public int getId()
    {
        return this.id;
    }

}