package com.example.jade.bouchette;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jade.client.Client;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Jade on 07/04/2015.
 * In this activity, the application :
 *      Checks the expecting answers if there are ones
 *      Displays text according to the answers and feedbacks
 *      Launchs the next exercise or return to the choice of exercises
 */
public class end_exercise extends ActionBarActivity implements TextToSpeech.OnInitListener{
    public Client client = null;
    public summary summary= new summary();
    private Button start = null;
    private TextView textview = null;
    private utils util;
    public TextToSpeech tts;
    public String text_read="";
    public ArrayList<response> response_list; //list of reponses awaited
    public ArrayList<response> response_student; //list of responses given by the student
    private int MY_DATA_CHECK_CODE = 0;
private Toast toast;
    /**
     * Button listener to continue the session of exercises
     */
    private View.OnClickListener buttonListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";no_erreur_balance\n";
            util.write(trace, getApplicationContext());
            Intent intent;
            if(util.demo && client.getIndex_summary().length == 0) //no more exercises in the demonstration session
            {
                    intent = new Intent(end_exercise.this, choice_exercise.class);
            }
            else
            {
                intent = new Intent(end_exercise.this, begin_exercise.class);
            }
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
            Bundle bundle = new Bundle();
            bundle.putSerializable("client", client);
            intent.putExtras(bundle);
            bundle.putSerializable("util", util);
            intent.putExtras(bundle);
            toast.cancel();
            startActivity(intent);
            finish();
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_exercice);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        client = (Client) bundle.getSerializable("client");
        util = (utils) bundle.getSerializable("util");
        response_list = (ArrayList<com.example.jade.bouchette.response>) intent.getSerializableExtra("response_list");
        response_student = (ArrayList<com.example.jade.bouchette.response>) intent.getSerializableExtra("response_student");
        textview = (TextView)findViewById(R.id.message);
        TextView end = (TextView)findViewById(R.id.end);
        util.setFont(end,getAssets());
        util.setFont(textview, getAssets());

        toast = new Toast(getApplicationContext());
        start = (Button)findViewById(R.id.debut);

        //adapt the text of the button
        if(util.demo && client.getIndex_summary().length == 0)
        {
            start.setText("Retour au choix des exercices");
        }
        else
        {
            start.setText("Exercice suivant");
        }

        util.setFont(start, getAssets());
        display_text();
        start.setOnClickListener(buttonListener);
    }

    /**
     * Text displayed changes according the responses expected and the ones given
     */
    private void display_text()
    {
        text_read ="";
        for (int i =0; i< response_list.size();i++)
        {
            int index = i+1;
            if (response_list.get(i).getType() && !response_list.get(i).getLetters().contentEquals(response_student.get(i).getLetters()))
            {
                text_read = text_read + "\n Tu devrais avoir " + response_list.get(i).getLetters()+" et tu as " +response_student.get(i).getLetters()+" dans la case "+index+".";
            }
            else if (!response_list.get(i).getType() && response_list.get(i).getNumber() !=(response_student.get(i).getNumber()))
            {
                {
                    text_read = text_read +"\n Tu devrais avoir " + response_list.get(i).getNumber()+" et tu as " +response_student.get(i).getNumber()+" dans la case "+index+".";
                }

            }
        }
        if(text_read.contentEquals(""))
        {
            if(response_list.size() == 0) text_read = "Tu as fini l'exercice";
            else if (response_list.size() == 1) text_read = "Bravo, tu as trouvé la bonne réponse.";
            else  text_read = "Bravo, tu as trouvé les bonnes réponses.";
        }
        textview.setText(text_read);
    }

@Override
public void onStart()
{
    super.onStart();
    if (tts == null) {
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
        tts = new TextToSpeech(this, this);
    }

    client.setActivity(3, end_exercise.this);
    //register the message receiverfrom the server
    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("my-event"));
}
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.FRANCE);
        }
    }
    @Override
    public  void onStop()
    {
        if (tts != null) {
            tts.stop();
        }
        super.onStop();
    }
    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
        System.gc();
    }

    /**
     * handle message from the server
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            util.setHandler(3,message, end_exercise.this,textview,tts, null, null,toast);
            client.message_continue = "";
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                if (tts == null){
                    System.out.println("tts est null");
                    //  tts = new TextToSpeech(this, this);
                    // tts.stop();
                }
            } else {
                // missing data, install it
                //ATTENTION: BELOW THIS GIVES ME PROBLEMS SINCE IT OPENS MARKET
                //AND I HAVE TO HIT THE BACK BUTTON, THEN, IT SPEAKS!
                //BTW TTS ENGINE "IS" INSTALLED!!
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }
}
