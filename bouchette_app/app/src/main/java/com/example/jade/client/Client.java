package com.example.jade.client;

import android.app.Activity;
import android.content.res.AssetManager;
import com.example.jade.bouchette.utils;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

/**
 * Created by jade on 12/06/15.
 * This class is the only way to communicate with the server
 * there is one instance of this class per student (so one for the entire application)
 * It is an abstract class
 */
public abstract class Client implements Serializable {

    public static Socket socket_reponse ; //socket for punctual request
    public static Socket socket_continue ; //socket for continuous listening
    public String message_response =""; // message after a request
    public String message_continue =""; // message receiving in continue
    public int id = 0;
    private int[] index_summary; //list of exercises (only used by the client_demo)

    public void setUtil(utils util){}

    public int[] getIndex_summary() {
        return index_summary;
    }

    public void setIndex_summary(int[] index_summary) {
        this.index_summary = index_summary;
    }

    /**
     * Opens the connexion with the server
     * @throws IOException if connexion errors
     */
    public void connect() throws IOException {}

    /**
     * The function sends the identifier of the student
     * @param id of the student
     * @throws IOException if connexion errors
     */
    public void setId(int id) throws IOException{ }

    /**
     * This function alert the server when the student changes the activity
     * @param activity_id identifier of the activity
     *                    0 => init
     *                    1 => begin_exercise
     *                    2 => exercise
     *                    3 => end_exercise
     * @param activity current activity
     */
    public void setActivity(int activity_id, Activity activity) { }

    /**
     * This function return the list of student
     * @param assetManager only used with the client_demo
     */
    public String getList(AssetManager assetManager) { return null; }

    /**
     * This function return the summary
     * @param assetManager only used with the client_demo
     */
    public void getSummary(AssetManager assetManager) throws IOException { }

    /**
     * This function closes the two opened sockets
     */
    public void close()
    {
        try {
            setId(4);
            socket_continue.close();
            socket_reponse.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
