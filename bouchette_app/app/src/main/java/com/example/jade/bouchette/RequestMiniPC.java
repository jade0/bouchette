package com.example.jade.bouchette;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

/**
 * This Task will get the JSON object send by the mini PC and parse it, in order to getthe number of scales and the content in each scale
 * Code partially found onthe internet
 */
class RequestMiniPC extends AsyncTask<ParametersRequest, scale, Void> {
    public static final String TAG = "Mon_TAG";
    private String urls;
    private  summary summary;
    private  TextView textview;
    private Integer response = 0;
    Activity activity;

    public RequestMiniPC(Activity activity) { this.activity = activity;}

    @Override
    protected Void doInBackground(ParametersRequest... parameters)
    {
        urls = parameters[0].getUrl();
        summary = parameters[0].getSummary();
        textview = parameters[0].getTextView();

        HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpGet = new HttpGet(urls);
            HttpResponse httpresponse = httpclient.execute(httpGet);
            HttpEntity httpentity = httpresponse.getEntity();
            if (httpentity != null) {
                InputStream inputstream = httpentity.getContent();
                BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputstream));
                StringBuilder strinbulder = new StringBuilder();
                String ligne = bufferedreader.readLine();
                while (ligne != null) {
                    strinbulder.append(ligne + "n");
                    ligne = bufferedreader.readLine();
                }
                bufferedreader.close();
                JSONObject jso = new JSONObject(strinbulder.toString());
                JSONObject jsomain = jso.getJSONObject("containers");
                response=jsomain.length();
                int number_scale = summary.getIStick().size();
                if (number_scale!= response)
                {
                    textview.setText("Tu dois avoir " + number_scale + " balances au début et tu en as " + response + ".");
                }

                for (Iterator iterator = jsomain.keys(); iterator.hasNext(); ) {
                    Object cle = iterator.next();
                    Object valcle = jsomain.get(String.valueOf(cle));
                    JSONObject bundles = (JSONObject) valcle;

                    JSONObject tmp = bundles.getJSONObject("nb_bundles");
                    String value = tmp.getString("value");

                    JSONObject tmp1 = bundles.getJSONObject("nb_units");
                    String value1 = tmp1.getString("value");

                    System.out.println("cle=" + cle + ", nb_bundles=" + value + ", nb_units ="+value1);

                    scale send = new scale(String.valueOf(cle),Integer.parseInt(value),Integer.parseInt(value1));
                    if (!summary.getIStick().contains(send)) {
                        onProgressUpdate(send);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            Intent intent = new Intent(activity, end_session.class);
            intent.putExtra("text","Problème de connexion avec le mini PC");
            activity.startActivity(intent);
            activity.finish();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void v)
    {
    }

    /**
     *
     * @param bal content of a scale
     */
    @Override
    protected void onProgressUpdate(scale... bal)
    {
        CharSequence deb = textview.getText();
        int nb_bundles = 0;
        int nb_units =0;
        if (deb != "") {
            deb = deb + "\n";
        }
        Iterator<scale> it = summary.getIStick().iterator();
        while (it.hasNext()) {
            scale b = it.next();
            if (b.getID().contentEquals(bal[0].getID()))
            {
                nb_bundles = b.getNb_bundles();
                nb_units = b.getNb_units();
            }
        }
        if(nb_bundles != bal[0].getNb_bundles()) {
            String foo;
            if (nb_bundles == 0)
            {
                foo = deb + "Tu ne dois pas avoir de paquets dans la balance " + bal[0].getID() + " au début et tu en as " + bal[0].getNb_bundles() + ".";
            }
            else {
                foo = deb + "Tu dois avoir " + nb_bundles + " paquets dans la balance " + bal[0].getID() + " au début et tu en as " + bal[0].getNb_bundles() + ".";
            }
            textview.setText(foo);
            deb = foo + "\n";
        }

        if(nb_units != bal[0].getNb_units()) {
            if (nb_units == 0) {
                textview.setText(deb + "Tu ne dois pas avoir de batonnets dans la balance " + bal[0].getID() + " au début et tu en as " + bal[0].getNb_units() + ".");
            } else {
                textview.setText(deb + "Tu dois avoir " + nb_units + " batonnets dans la balance " + bal[0].getID() + " au début et tu en as " + bal[0].getNb_units() + ".");
            }
        }
        CharSequence bar = textview.getText();
       /* if(bar.length() > 250) {
            textview.setTextSize(20);
        }
        else {textview.setTextSize(30);}*/
    }

}
