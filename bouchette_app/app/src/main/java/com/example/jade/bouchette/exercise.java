package com.example.jade.bouchette;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jade.client.Client;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Jade on 07/04/2015.
 * In this activity, the application :
 *      Displays an interface according to the parameters of the exercise
 *      Registers the answersof the student if there are ones
 *      Transfers the feedbacks from the server
 */
public class exercise extends ActionBarActivity implements TextToSpeech.OnInitListener {
    public String text = "Voici l'énoncé de l'exercise";
    public String text_read = "";
    public ArrayList<response> response_list; //list of responses expected
    public ArrayList<response> response_student; // list of responses given by the student
    private ArrayList<EditText> response ; //list of edit text (response fields)
    public TextView wording = null;
    private Button send = null;
    private String spoken = "";
    private Button speak = null;
    private Client client;
    public TextToSpeech tts;
    private utils util;
    private int response_number =0;
    private int MY_DATA_CHECK_CODE = 0;
private Toast toast;
    /**
     * Button listener to submit the answers
     */
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            boolean check = true;
            // check if every reponses fields are filled
            for (int i =0; i< response_list.size();i++)
            {
                if  (response.get(i).getText().length() == 0)
                {
                    check = false;
                }
            }

            //if the previous check has failed, displays a toast to inform the student
            if (response_number != 0 && !check) {
                util.setToast("Tu dois entrer ta réponse",exercise.this, toast);

                //write traces
                String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";click_response_null\n";
                util.write(trace, getApplicationContext());
            }
            else //if the check succed, creates a list of reponses
            {
                for(int i =0; i< response_number;i++)
                {
                    response tmp;
                    if (response_list.get(i).getType()) {  tmp = new response(false, 0,response.get(i).getText().toString());}
                    else  { tmp = new response(false, Integer.parseInt(response.get(i).getText().toString()),""); }
                    response_student.add(tmp);
                }

                spoken ="null";
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                //writes traces
                String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";click_response\n";
                util.write(trace, getApplicationContext());
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);

                //send datas to the next activity and launch it
                Intent intent = new Intent(exercise.this, end_exercise.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("client", client);
                intent.putExtras(bundle);
                bundle.putSerializable("util", util);
                intent.putExtras(bundle);
                intent.putExtra("response_list", response_list);
                intent.putExtra("response_student", response_student);
                toast.cancel();
                startActivity(intent);
                finish();
            }
        }
    };

    /**
     * Button listener to listen the wording
     */
    private View.OnClickListener speakListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            tts.stop();
            tts.speak(spoken, TextToSpeech.QUEUE_ADD, null);
            //writes traces
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";click_listen\n";
            util.write(trace, getApplicationContext());
        }
    };

    /**
     * Watchers for each response field
     */
    private TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //envoyer chacun des evenements
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";enter_response1;" + s.toString() + "\n";
            util.write(trace, getApplicationContext());
        }
        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher watcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //envoyer chacun des evenements
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";enter_response2;" + s.toString() + "\n";
            util.write(trace, getApplicationContext());
        }
        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher watcher3 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //envoyer chacun des evenements
            String trace = client.id + ";" + new Timestamp(System.currentTimeMillis()) + ";enter_response3;" + s.toString() + "\n";
            util.write(trace, getApplicationContext());
        }
        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        response =new ArrayList<EditText>();
        response_list= new ArrayList<response>();
        response_student= new ArrayList<response>();

      Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        client = (Client) bundle.getSerializable("client");
        util = (utils) bundle.getSerializable("util");
        response_list = (ArrayList<com.example.jade.bouchette.response>) intent.getSerializableExtra("response_list");
        text = intent.getStringExtra("text");
        spoken = intent.getStringExtra("vocal");
        wording = (TextView) findViewById(R.id.summary);
         toast = new Toast(getApplicationContext());

        if(!text.contentEquals("null")) //if there is a written wording
            wording.setText(text);
        util.setFont(wording, getAssets());

        send = (Button) findViewById(R.id.button);
        send.setOnClickListener(buttonListener);
        util.setFont(send, getAssets());

        response_number = response_list.size();

        //add response field to the list response
        EditText response1 = (EditText) findViewById(R.id.response1);
        addToList(response1, watcher1);
        EditText response2 = (EditText) findViewById(R.id.response2);
        addToList(response2, watcher2);
        EditText   response3 = (EditText) findViewById(R.id.response3);
        addToList(response3, watcher3);

        speak = (Button) findViewById(R.id.button_spoken);
        speak.setOnClickListener(speakListener);
        util.setFont(speak, getAssets());

        if (!spoken.contentEquals("null")) {
            speak.setLayoutParams(new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 0f));
            TableLayout.LayoutParams table = new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 2f);
            send.setLayoutParams(table);
        }
        else {speak.setVisibility(View.INVISIBLE);}

        //add input filter for all response fields
        for (int i =0; i < response_list.size();i++)
        {
            System.out.println("type " + response_list.get(i).getType());
            response.get(i).setVisibility(View.VISIBLE);
            InputMethodManager iMM = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            iMM.hideSoftInputFromWindow(response1.getWindowToken(), 0);
            setFilter(response_list.get(i).getType(), response.get(i));
        }
        setSpeech();
    }

    /**
     * Initialize an edit text and link it with a watcher
     * @param resp edit text to modify
     * @param watcher watcher to link
     */
    public void addToList(EditText resp,TextWatcher watcher)
    {
        resp.setTextSize(30);
        resp.setVisibility(View.INVISIBLE);
        resp.addTextChangedListener(watcher);
        response.add(resp);
    }

    /**
     *
     */
    public void setSpeech() {
        if (tts == null) {
            Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
            tts = new TextToSpeech(this, this);
        }
        if (!spoken.contentEquals("null")) {
            speak.setVisibility(View.VISIBLE);
            TableLayout.LayoutParams table = new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 2f);
            table.setMargins(0, 0, 0, 16);
            send.setLayoutParams(table);
            speak.setLayoutParams(new TableLayout.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, 0, 1f));
            text_read = spoken;
        } else {
            if (tts != null)   tts.stop();
            speak.setVisibility(View.INVISIBLE);
        }
    }

    public void setFilter(boolean letters, EditText edit)
    {
        InputFilter filter;
        edit.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        if (letters) {
            filter = new InputFilter() {
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    for (int i = start; i < end; i++) {
                        if (!Character.isLetter(source.charAt(i))) {
                            return "";
                        }
                    }
                    return null;
                }
            };
        }
        else
        {
            filter = new InputFilter() {
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    for (int i = start; i < end; i++) {
                        if (!Character.isDigit(source.charAt(i))) {
                            return "";
                        }
                    }
                    return null;
                }
            };
        }
        edit.setFilters(new InputFilter[]{filter});
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
        client.setActivity(2, exercise.this);
    }

    /**
     * handle message from the server
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            util.setHandler(2   , message, exercise.this, wording, tts, speak, send,toast);
            client.message_continue = "";
        }
    };



    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.FRANCE);
            if(!spoken.contentEquals("null"))
            tts.speak(spoken, TextToSpeech.QUEUE_ADD, null);
        }
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
        System.gc();
    }
    @Override
    public  void onStop()
    {
        if (tts != null) {
            tts.stop();
        }

        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                if (tts == null){
                    System.out.println("tts est null");
                    //  tts = new TextToSpeech(this, this);
                    // tts.stop();
                }
            } else {
                // missing data, install it
                //ATTENTION: BELOW THIS GIVES ME PROBLEMS SINCE IT OPENS MARKET
                //AND I HAVE TO HIT THE BACK BUTTON, THEN, IT SPEAKS!
                //BTW TTS ENGINE "IS" INSTALLED!!
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

}
