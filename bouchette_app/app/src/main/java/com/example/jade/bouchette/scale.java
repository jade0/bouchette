package com.example.jade.bouchette;

/**
 * Created by jade on 28/05/15.
 * This class represent an entity scale, which is identified by an id, and contains bundles and sticks.
 */
public class scale {
    private String id = null; //scale name
    private int nb_bundles = 0;
    private int nb_units =0;

    public scale(String id, int nb_bundles, int nb_units) {
        this.id =id;
        this.nb_bundles = nb_bundles;
        this.nb_units = nb_units;
    }

    /**
     * @return name of the scale
     */
    public String getID() {return this.id;}

    /**
     * @return number of bundles
     */
    public int getNb_bundles() {return this.nb_bundles;}

    /**
     * @return number of units
     */
    public int getNb_units() {return this.nb_units;}
}
