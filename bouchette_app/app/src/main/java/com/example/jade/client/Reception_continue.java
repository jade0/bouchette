package com.example.jade.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import com.example.jade.bouchette.end_session;

import java.io.*;

/**
 * This thread is running to get the feedback from the server
 */
public class Reception_continue implements Runnable {

    private boolean cancel = false;
    private BufferedReader in = null;
    private Client clientReel;
    private Handler handler;
    private  Activity activity;

    public Reception_continue(BufferedReader in, Client clientReel, Handler handler, Activity activity){
        this.in = in;
        this.clientReel = clientReel;
        this.handler = handler;
        this.activity = activity;
    }

    public void run() {

            while(!cancel){
                try {
                    clientReel.message_continue = in.readLine();
                    System.out.println("Le serveur vous dit : " + clientReel.message_continue);
                    if(clientReel.message_continue == null) {
                        System.out.println("je dois planter");
                        Intent intent = new Intent(activity, end_session.class);
                        clientReel.close();
                        intent.putExtra("text","Probleme de connexion avec le serveur");
                        activity.startActivity(intent);
                        activity.finish();
                        cancel();
                    }
                  handler.sendEmptyMessage(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

    public void cancel()
    {
        handler.removeCallbacksAndMessages(this);
        this.cancel = true;
    }
}