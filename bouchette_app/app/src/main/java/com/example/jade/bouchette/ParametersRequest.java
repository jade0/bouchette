package com.example.jade.bouchette;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by jade on 22/05/15.
 * This class is used to facilitate the acces to the tangible device
 * it contains all usefull informations
 */
public class ParametersRequest {
    private String url;
    private summary summary;
    private TextView textView;

    /**
     * @param url of the mini pc
     * @param summary of the current exercise
     * @param textView to display informations text
     */
    public ParametersRequest(String url, summary summary, TextView textView)
    {
        this.url = url;
        this.summary = summary;
        this.textView = textView;
    }

    public String getUrl() {
        return this.url;
    }

    public summary getSummary() {
        return this.summary;
    }

    public TextView getTextView() {
        return this.textView;
    }
}
